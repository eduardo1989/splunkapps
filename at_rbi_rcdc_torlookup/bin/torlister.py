import requests,sys,time,os,threading
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import splunk.Intersplunk


current_time=time.time()

def lookup_dl():
    global torips
    sess = requests.Session()
    retries = Retry(total=5, backoff_factor=1, status_forcelist=[ 502, 503, 504 ])
    sess.mount('https://', HTTPAdapter(max_retries=retries))
    with open("/opt/splunk/etc/apps/at_rbi_rcdc_torlookup/bin/.currtime","w") as f1:
        f1.write(str(time.time()))
    rp = sess.get("https://www.dan.me.uk/torlist/")
    # rp = sess.get("https://check.torproject.org/torbulkexitlist")
    torips = rp.text.split("\n")
    if len(torips) > 1:
        with open("/opt/splunk/etc/apps/at_rbi_rcdc_torlookup/lookups/torlist.csv","w") as f:
            f.write("ip"+'\n')
            for z in torips:
                f.write(z+'\n')


def lookuping_se(ip,results,torips):
    global newresults
    newresults = []
    for r in results:
        for y in torips:
            if r[ip] == y:
                r.update({'is_tor':True,'torlink':'https://metrics.torproject.org/rs.html#search/'+y})
            else:
                continue
        newresults.append(r)

def lookuping_so(ip,results,torips):
    global newresults
    newresults = []
    for r in results:
        for y in torips:
            if r[ip] == y:
                r.update({'is_tor':True,'torlink':'https://metrics.torproject.org/rs.html#search/'+y})
                newresults.append(r)
            else:
                continue


ip = sys.argv[1]
try:
    show_only = sys.argv[2]
    show_only = show_only.split('=')
except:
    show_only = "2"

results,_,_ = splunk.Intersplunk.getOrganizedResults()

if os.path.exists("/opt/splunk/etc/apps/at_rbi_rcdc_torlookup/bin/.currtime") is True:
    oldf = open("/opt/splunk/etc/apps/at_rbi_rcdc_torlookup/bin/.currtime","r")
    old_time = oldf.read()
else:
    old_time = 0


if float(old_time)+1800 < current_time:
    lookup_dl()
else:
    torips = open("/opt/splunk/etc/apps/at_rbi_rcdc_torlookup/lookups/torlist.csv","r")
    torips = torips.read().split('\n')[1:]


if (show_only[-1] == "t") or (show_only[-1] == "true") or (show_only[-1] == "1"):
    lookuping_so(ip,results,torips)
elif (show_only[-1] == "f") or (show_only[-1] == "false") or (show_only[-1] == "0"):
    lookuping_se(ip,results,torips)
elif show_only == "2":
    lookuping_se(ip,results,torips)
else:
    print("Invalid argument")
    print("use one of these: t or true or 1 / f or false or 0")

splunk.Intersplunk.outputResults(newresults)